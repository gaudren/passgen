""" Password generator
    2 Upper, 2 Lower, 2 Number, 2 Special"""

import random

upperList = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
lowerList = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
numList = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
specList = ["!", "@", "#", "$", "%", "^", "&", "*"]

count = 1
order = []
while count < 9:
    if count <= 2:
        order.append(random.choice(upperList))
        count += 1
    elif count > 2 and count <= 4:
        order.append(random.choice(lowerList))
        count += 1
    elif count > 4 and count <= 6:
        order.append(random.choice(numList))
        count += 1
    else:
        order.append(random.choice(specList))
        count += 1

random.shuffle(order)
final = ''.join(order)

print("Generated password: " + final)

